<?php

namespace Drupal\monolog_dblog\Processor;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Unicode;

class RemoveBacktraceProcessor {

  public function __invoke(array $record) {

    if (isset($record['context']['@backtrace_string'])) {
      $new_message = PlainTextOutput::renderFromHtml((string) new FormattableMarkup('%type: @message in %function (line %line of %file).', $record['context']));

      $select = \Drupal::database()->select('watchdog', 'w');
      $select->addField('w', 'wid');
      $select->condition('timestamp', $record['datetime']->getTimestamp());
      $select->condition('type', Unicode::substr($record['channel'], 0, 64));
      $results = $select->execute()->fetchAll();

      unset($record['context']['@backtrace_string']);

      $record['message'] = $new_message;
      if (!empty($results) && is_array($results)) {
        $record['extra']['more_infos'] = \Drupal::urlGenerator()
          ->generateFromRoute('dblog.event', ['event_id' => $results[0]->wid]);
      }
    }

    return $record;
  }
}