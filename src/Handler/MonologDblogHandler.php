<?php

namespace Drupal\monolog_dblog\Handler;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\monolog\Logger\MonologLogLevel;
use Monolog\Handler\AbstractProcessingHandler;
use Psr\Log\LoggerInterface;

class MonologDblogHandler extends AbstractProcessingHandler {

  protected $logger;

  protected $levelTranslation = array(
    MonologLogLevel::EMERGENCY => RfcLogLevel::EMERGENCY,
    MonologLogLevel::ALERT => RfcLogLevel::ALERT,
    MonologLogLevel::CRITICAL => RfcLogLevel::CRITICAL,
    MonologLogLevel::ERROR => RfcLogLevel::ERROR,
    MonologLogLevel::WARNING => RfcLogLevel::WARNING,
    MonologLogLevel::NOTICE => RfcLogLevel::NOTICE,
    MonologLogLevel::INFO => RfcLogLevel::INFO,
    MonologLogLevel::DEBUG => RfcLogLevel::DEBUG,
  );


  public function __construct(LoggerInterface $logger) {
    parent::__construct();
    $this->logger = $logger;
  }

  protected function write(array $record) {
    $level = $record['level'];
    if (array_key_exists($level, $this->levelTranslation)) {
      $level = $this->levelTranslation[$level];
    }

    $context = $record['context'];
    $context = array_merge($context, $record['extra']);
    $context['channel'] = $record['channel'];
    $context['link'] = '';
    $context['timestamp'] = $record['datetime']->getTimestamp();
    $this->logger->log($level, $record['message'], $context);
  }
}