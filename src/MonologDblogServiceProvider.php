<?php

namespace Drupal\monolog_dblog;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

class MonologDblogServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('logger.dblog')) {

      $element = $container->getDefinition('logger.dblog');
      $element->clearTags();

      $service_defintion = $container->register('monolog.handler.dblog', 'Drupal\monolog_dblog\Handler\MonologDblogHandler');
      $service_defintion->addArgument(new Reference('logger.dblog'));

      $container->register('monolog.processor.remove_backtrace', 'Drupal\monolog_dblog\Processor\RemoveBacktraceProcessor');
    }
  }
}